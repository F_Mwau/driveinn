package com.fmwau.driveinn.dbadapters;

public class FuelTrans {
    private String fuelPeriod;
    private String fueltransDate;
    private String fueltransLocation;
    private String fueltransAmount;



    public String getFueltransDate() {
        return fueltransDate;
    }

    public void setFueltransDate(String fueltransDate) {
        this.fueltransDate = fueltransDate;
    }

    public String getFueltransLocation() {
        return fueltransLocation;
    }

    public void setFueltransLocation(String fueltransLocation) {
        this.fueltransLocation = fueltransLocation;
    }

    public String getFueltransAmount() {
        return fueltransAmount;
    }

    public void setFueltransAmount(String fueltransAmount) {
        this.fueltransAmount = fueltransAmount;
    }

    public String getFuelPeriod() {
        return fuelPeriod;
    }

    public void setFuelPeriod(String fuelPeriod) {
        this.fuelPeriod = fuelPeriod;
    }
}
