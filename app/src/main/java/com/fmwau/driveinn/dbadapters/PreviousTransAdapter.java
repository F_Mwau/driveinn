package com.fmwau.driveinn.dbadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fmwau.driveinn.R;

import java.util.List;

public class PreviousTransAdapter extends RecyclerView.Adapter<PreviousTransAdapter.ViewHolder>{

    Context context;
    List<PreviousTrans> getDataAdapter;


    public PreviousTransAdapter(List<PreviousTrans> getDataAdapter, Context context) {
        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;
    }


    @NonNull
    @Override
    public PreviousTransAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_list_design, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PreviousTransAdapter.ViewHolder holder, int position) {
        final PreviousTrans getDataAdapter1 = getDataAdapter.get(position);
        holder.fuelAmount.setText("Ksh. "+getDataAdapter1.getTransAmount());
        holder.fuelLocation.setText(getDataAdapter1.getTransLocation());
        holder.fuelDate.setText(getDataAdapter1.getTransDate());

    }

    @Override
    public int getItemCount() {
        return getDataAdapter.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView fuelLocation;
        public TextView fuelAmount;
        public TextView fuelDate;
        public ViewHolder(View itemView) {
            super(itemView);
            fuelLocation = (TextView) itemView.findViewById(R.id.locationTxt);
            fuelAmount = (TextView) itemView.findViewById(R.id.amountTxt);
            fuelDate = (TextView) itemView.findViewById(R.id.dateTxt);
        }
    }
}
