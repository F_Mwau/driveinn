package com.fmwau.driveinn.dbadapters;

import java.util.ArrayList;

public class FuelPeriodTrans {
    private String fuelPeriod;
    private String transcount;

    public String getFuelPeriod() {
        return fuelPeriod;
    }

    public void setFuelPeriod(String fuelPeriod) {
        this.fuelPeriod = fuelPeriod;
    }

    public String getTranscount() {
        return transcount;
    }

    public void setTranscount(String transcount) {
        this.transcount = transcount;
    }
}
