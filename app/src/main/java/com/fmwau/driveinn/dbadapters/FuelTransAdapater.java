package com.fmwau.driveinn.dbadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fmwau.driveinn.R;

import java.util.List;

public class FuelTransAdapater extends RecyclerView.Adapter<FuelTransAdapater.ViewHolder>{

    Context context;
    List<FuelTrans> getDataAdapter;


    public FuelTransAdapater(List<FuelTrans> getDataAdapter, Context context) {
        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;
    }

    @NonNull
    @Override
    public FuelTransAdapater.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fuel_list_design, parent, false);
        FuelTransAdapater.ViewHolder viewHolder = new FuelTransAdapater.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FuelTransAdapater.ViewHolder holder, int position) {
        final FuelTrans getDataAdapter1 = getDataAdapter.get(position);
        holder.ffuelAmount.setText("Ksh. "+getDataAdapter1.getFueltransAmount());
        holder.ffuelLocation.setText(getDataAdapter1.getFueltransLocation());
        holder.ffuelDate.setText(getDataAdapter1.getFueltransDate());

    }

    @Override
    public int getItemCount() {
        return getDataAdapter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView ffuelLocation;
        public TextView ffuelAmount;
        public TextView ffuelDate;
        public ViewHolder(View itemView) {
            super(itemView);
            ffuelLocation = (TextView) itemView.findViewById(R.id.flocationTxt);
            ffuelAmount = (TextView) itemView.findViewById(R.id.famountTxt);
            ffuelDate = (TextView) itemView.findViewById(R.id.fdateTxt);
        }
    }
}
