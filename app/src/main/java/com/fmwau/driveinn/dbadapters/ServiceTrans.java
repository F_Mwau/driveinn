package com.fmwau.driveinn.dbadapters;

public class ServiceTrans {

    private String serviceDesc;
    private String servicetransDate;
    private String serviceLocation;
    private String servceAmount;

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public String getServicetransDate() {
        return servicetransDate;
    }

    public void setServicetransDate(String servicetransDate) {
        this.servicetransDate = servicetransDate;
    }

    public String getServiceLocation() {
        return serviceLocation;
    }

    public void setServiceLocation(String serviceLocation) {
        this.serviceLocation = serviceLocation;
    }

    public String getServceAmount() {
        return servceAmount;
    }

    public void setServceAmount(String servceAmount) {
        this.servceAmount = servceAmount;
    }
}
