package com.fmwau.driveinn.dbadapters;

public class PreviousTrans {
    String transDate;
    String transLocation;
    String transAmount;

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTransLocation() {
        return transLocation;
    }

    public void setTransLocation(String transLocation) {
        this.transLocation = transLocation;
    }

    public String getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(String transAmount) {
        this.transAmount = transAmount;
    }
}
