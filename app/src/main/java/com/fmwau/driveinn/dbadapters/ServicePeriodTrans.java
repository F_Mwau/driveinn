package com.fmwau.driveinn.dbadapters;

public class ServicePeriodTrans {

    private String servicePeriod;
    private String servicetranscount;

    public String getServicePeriod() {
        return servicePeriod;
    }

    public void setServicePeriod(String servicePeriod) {
        this.servicePeriod = servicePeriod;
    }

    public String getServicetranscount() {
        return servicetranscount;
    }

    public void setServicetranscount(String servicetranscount) {
        this.servicetranscount = servicetranscount;
    }
}
