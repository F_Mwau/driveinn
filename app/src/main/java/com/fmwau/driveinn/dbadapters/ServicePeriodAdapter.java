package com.fmwau.driveinn.dbadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fmwau.driveinn.R;

import java.util.List;

public class ServicePeriodAdapter extends RecyclerView.Adapter<ServicePeriodAdapter.ViewHolder> {
    Context context;
    List<ServicePeriodTrans> headetValue;

    public ServicePeriodAdapter(List<ServicePeriodTrans> getDataAdapter, Context context) {
        super();
        this.headetValue = getDataAdapter;
        this.context = context;
    }

    @NonNull
    @Override
    public ServicePeriodAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_period, parent, false);
        ServicePeriodAdapter.ViewHolder viewHolder = new ServicePeriodAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ServicePeriodAdapter.ViewHolder holder, int position) {
        final ServicePeriodTrans getDataAdapter1 = headetValue.get(position);
        holder.speriodtxt.setText(getDataAdapter1.getServicePeriod());
        holder.stranscounttxt.setText(getDataAdapter1.getServicetranscount());


    }

    @Override
    public int getItemCount() {
        return headetValue.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView speriodtxt;
        public TextView stranscounttxt;
        public ViewHolder(View itemView) {
            super(itemView);
            speriodtxt = (TextView) itemView.findViewById(R.id.serviceperiodtxt);
            stranscounttxt = (TextView) itemView.findViewById(R.id.servicetranscounttxt);

        }
    }
}
