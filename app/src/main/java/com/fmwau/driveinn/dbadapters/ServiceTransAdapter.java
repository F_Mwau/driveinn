package com.fmwau.driveinn.dbadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fmwau.driveinn.R;

import java.util.List;

public class ServiceTransAdapter extends RecyclerView.Adapter<ServiceTransAdapter.ViewHolder>{

    Context context;
    List<ServiceTrans> getDataAdapter;


    public ServiceTransAdapter(List<ServiceTrans> getDataAdapter, Context context) {
        super();
        this.getDataAdapter = getDataAdapter;
        this.context = context;
    }

    @NonNull
    @Override
    public ServiceTransAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.serivice_list_design, parent, false);
        ServiceTransAdapter.ViewHolder viewHolder = new ServiceTransAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceTransAdapter.ViewHolder holder, int position) {
        final ServiceTrans getDataAdapter1 = getDataAdapter.get(position);
        holder.serviceAmount.setText("Ksh. "+getDataAdapter1.getServceAmount());
        holder.serviceLocation.setText(getDataAdapter1.getServiceLocation());
        holder.serviceDate.setText(getDataAdapter1.getServicetransDate());
        holder.serviceDesc.setText(getDataAdapter1.getServiceDesc());

    }

    @Override
    public int getItemCount() {
        return getDataAdapter.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView serviceLocation;
        public TextView serviceAmount;
        public TextView serviceDate;
        public TextView serviceDesc;
        public ViewHolder(View itemView) {
            super(itemView);
            serviceDate = (TextView) itemView.findViewById(R.id.servicedateTxt);
            serviceLocation = (TextView) itemView.findViewById(R.id.servicelocationTxt);
            serviceDesc = (TextView) itemView.findViewById(R.id.servicedesctxt);
            serviceAmount = (TextView) itemView.findViewById(R.id.serviceamountTxt);
        }
    }
}
