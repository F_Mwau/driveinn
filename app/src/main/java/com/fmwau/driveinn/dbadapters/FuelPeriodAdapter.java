package com.fmwau.driveinn.dbadapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fmwau.driveinn.R;

import java.util.List;

public class FuelPeriodAdapter extends RecyclerView.Adapter<FuelPeriodAdapter.ViewHolder> {
    Context context;
    List<FuelPeriodTrans> headetValue;

    public FuelPeriodAdapter(List<FuelPeriodTrans> getDataAdapter, Context context) {
        super();
        this.headetValue = getDataAdapter;
        this.context = context;
    }

    @NonNull
    @Override
    public FuelPeriodAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fuel_period, parent, false);
        FuelPeriodAdapter.ViewHolder viewHolder = new FuelPeriodAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FuelPeriodAdapter.ViewHolder holder, int position) {
        final FuelPeriodTrans getDataAdapter1 = headetValue.get(position);
        holder.periodtxt.setText(getDataAdapter1.getFuelPeriod());
        holder.transcounttxt.setText(getDataAdapter1.getTranscount());


    }

    @Override
    public int getItemCount() {
        return headetValue.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView periodtxt;
        public TextView transcounttxt;
        public ViewHolder(View itemView) {
            super(itemView);
            periodtxt = (TextView) itemView.findViewById(R.id.periodtxt);
            transcounttxt = (TextView) itemView.findViewById(R.id.transcounttxt);

        }
    }
}
