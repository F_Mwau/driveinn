package com.fmwau.driveinn;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fmwau.driveinn.DashBoard_and_Fragments.DashBoard;
import com.fmwau.driveinn.DashBoard_and_Fragments.HomeFragment;
import com.fmwau.driveinn.Registration.Login;
import com.fmwau.driveinn.extra.Globalconstants;

public class MainActivity extends AppCompatActivity {

    TextView nameTxt, descTxt;
    RelativeLayout relativeLayout;
    Animation txtAnimation, layoutAnimation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        hooks();
    }

    private void hooks() {
        nameTxt = findViewById(R.id.appNameTxt);
        relativeLayout = findViewById(R.id.splashLayout);
        txtAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fall_down);
        layoutAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bottom_to_top);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                relativeLayout.setVisibility(View.VISIBLE);
                relativeLayout.setAnimation(layoutAnimation);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        nameTxt.setVisibility(View.VISIBLE);
                        nameTxt.setAnimation(txtAnimation);

                    }
                }, 900);
            }
        }, 500);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
                String userId = sharedPreferences.getString("userid","");
                if(userId.equalsIgnoreCase("")) {
                    Intent i = new Intent(getApplicationContext(), Login.class);
                    startActivity(i);
                    finish();
                }
                else{
                    Intent splash = new Intent(getApplicationContext(), DashBoard.class);
                    startActivity(splash);
                    finish();
                }

            }
        }, 5500);
    }
}