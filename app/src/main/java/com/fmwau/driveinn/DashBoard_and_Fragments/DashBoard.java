package com.fmwau.driveinn.DashBoard_and_Fragments;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;

import com.fmwau.driveinn.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class DashBoard extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        hooks();
    }

    private void hooks() {
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        getSupportFragmentManager().beginTransaction().replace(R.id.dashBoardLayout, new HomeFragment()).commit();
        bottomNavigationView.setSelectedItemId(R.id.nav_home);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                switch (item.getItemId()){
                    case R.id.nav_home:
                        fragment = new HomeFragment();
                    break;
                    case R.id.nav_reports:
                        fragment = new ReportsFragment();
                    break;
                    case R.id.nav_more:
                        fragment = new ProfileFragment();
                    break;
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.dashBoardLayout, fragment).commit();

                return true;
            }
        });
    }
}