package com.fmwau.driveinn.DashBoard_and_Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fmwau.driveinn.R;
import com.fmwau.driveinn.Registration.Login;
import com.fmwau.driveinn.Task.AddCarActivity;
import com.fmwau.driveinn.Task.RefuelActivity;
import com.fmwau.driveinn.Task.ServiceActivity;
import com.fmwau.driveinn.extra.Globalconstants;
import com.google.android.material.card.MaterialCardView;


public class ProfileFragment extends Fragment {

    ImageView profilePic,logoutImg;
    TextView nameProfile,emailProfile,mobileProfile;
    CardView uploadPicBtn;;
    int SELECT_IMAGE_CODE = 1;
   // AlertDialog.Builder builder;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
        profilePic = view.findViewById(R.id.profile_picture);
        uploadPicBtn = view.findViewById(R.id.profile_pic_edit);
        logoutImg = view.findViewById(R.id.logout);

        nameProfile = view.findViewById(R.id.profilename);
        emailProfile = view.findViewById(R.id.profileemail);
        mobileProfile = view.findViewById(R.id.profiletel);

        String fullname =  sharedPreferences.getString("customername","");
        String email = sharedPreferences.getString("email","");
        String mobile = sharedPreferences.getString("mobile","");
        System.out.println("fullname====>>>>"+fullname);
        nameProfile.setText(fullname.toString());
        emailProfile.setText(email);
        mobileProfile.setText(mobile);


        uploadPicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Title"),SELECT_IMAGE_CODE);
            }
        });

        logoutImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.dialog_message)
                        .setTitle(R.string.dialog_title);


                builder.setMessage("Do you want to logout from the application ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    SharedPreferences sharedPreferences = getContext().getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.clear();
                                    editor.commit();
                                    Intent i = new Intent(getContext(), Login.class);
                                    startActivity(i);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        })
                        .setNegativeButton(R.string.logout_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });

                AlertDialog alert11 = builder.create();
                alert11.show();


            }
        });
        return view;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            Uri uri = data.getData();
            profilePic.setImageURI(uri);
        }
    }
}