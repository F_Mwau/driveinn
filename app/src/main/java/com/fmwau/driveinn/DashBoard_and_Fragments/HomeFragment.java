package com.fmwau.driveinn.DashBoard_and_Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.fmwau.driveinn.Adapters.ParentRecyclerAdapter;
import com.fmwau.driveinn.Adapters.SliderAdapter;
import com.fmwau.driveinn.Adapters.ViewPagerAdapter;
import com.fmwau.driveinn.R;
import com.fmwau.driveinn.Task.AddCarActivity;
import com.fmwau.driveinn.Task.RefuelActivity;
import com.fmwau.driveinn.Task.ServiceActivity;
import com.fmwau.driveinn.dbadapters.PreviousTrans;
import com.fmwau.driveinn.dbadapters.PreviousTransAdapter;
import com.fmwau.driveinn.extra.CustomVolleyRequest;
import com.fmwau.driveinn.extra.Globalconstants;
import com.fmwau.driveinn.extra.SliderUtils;
import com.fmwau.driveinn.models.ChildList;
import com.fmwau.driveinn.models.ParentList;
import com.fmwau.driveinn.models.SliderItem;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.material.card.MaterialCardView;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private AdView adViews;
    private RecyclerView homeRecycler;
    ViewPager2 viewPager2;
    private Handler sliderHandler = new Handler();
    MaterialCardView refuelCard, carCard, serviceCard;
    TextView userNameTxt;
    ImageView ivOutput;
    private Context context;

    List<PreviousTrans> GetDataAdapter1;

    boolean doubleBackToExitPressedOnce = false;

    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;
    ProgressBar progressBar;

    JsonArrayRequest jsonArrayRequest ;
    RequestQueue requestQueue ;

    String JSON_FUELAMT = "fuelamount";
    String JSON_FUELLOC= "fuellocation";
    String JSON_FUELDATE= "fueldate";

    //for image banner slide
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    List<SliderUtils> sliderImg;
    ViewPagerAdapter viewPagerAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        requestQueue = CustomVolleyRequest.getInstance(this.getActivity()).getRequestQueue();

        sliderImg = new ArrayList<>();

        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
       String userBarCode = sharedPreferences.getString("barcode", "");
        String fullname = sharedPreferences.getString("customername", "");
       // homeRecycler = view.findViewById(R.id.homeRecyclerView);
        viewPager2 = view.findViewById(R.id.imageSliderViewPager);
        sliderDotspanel = (LinearLayout)view.findViewById(R.id.SliderDots);


        refuelCard = view.findViewById(R.id.home_refuel_card);
        carCard = view.findViewById(R.id.home_add_car_card);
        serviceCard = view.findViewById(R.id.home_service_card);
        userNameTxt = view.findViewById(R.id.userNameTxt);
        ivOutput  = view.findViewById(R.id.qr_code);
        userNameTxt.setText(fullname);


        GetDataAdapter1 = new ArrayList<>();
        homeRecycler = view.findViewById(R.id.homeRecyclerView);
//        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        homeRecycler.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(view.getContext());
        homeRecycler.setLayoutManager(recyclerViewlayoutManager);

        JSON_DATA_WEB_CALL(sharedPreferences.getString("userid",""));



        //generate the user qrcode
        MultiFormatWriter writer = new MultiFormatWriter();
        try {
            //initialize bit matrix
            BitMatrix matrix = writer.encode(userBarCode, BarcodeFormat.QR_CODE,350, 350);
            //Initialize barcode encoder
            BarcodeEncoder encoder = new BarcodeEncoder();
            //Initialize bitmap
            Bitmap bitmap = encoder.createBitmap(matrix);
            //add bitmat to imageviewer
            ivOutput.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        refuelCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent refuelCar = new Intent(getContext(), RefuelActivity.class);
                startActivity(refuelCar);
            }
        });

        carCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent carService = new Intent(getContext(), AddCarActivity.class);
                startActivity(carService);
            }
        });
        serviceCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent serviceCar = new Intent(getContext(), ServiceActivity.class);
                startActivity(serviceCar);
            }
        });

//
//        viewPager2.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//                for(int i = 0; i< dotscount; i++){
//                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.default_dot));
//                }
//
//                dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.selected_dot));
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });



        advertViewPagerSlider();

        return view;
    }




    private void advertViewPagerSlider() {
        List<SliderItem> sliderItems = new ArrayList<>();
        sliderItems.add(new SliderItem(R.drawable.lotus));
        sliderItems.add(new SliderItem(R.drawable.lotus));
        sliderItems.add(new SliderItem(R.drawable.lotus));
        sliderItems.add(new SliderItem(R.drawable.lotus));
        sliderItems.add(new SliderItem(R.drawable.lotus));

        viewPager2.setAdapter(new SliderAdapter(sliderItems, viewPager2));
        viewPager2.setClipToPadding(false);
        viewPager2.setClipChildren(false);
        viewPager2.setOffscreenPageLimit(3);
        viewPager2.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(40));
        compositePageTransformer.addTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float r = 1 - Math.abs(position);
                page.setScaleY(0.85f + r * 0.15f);
            }
        });
        viewPager2.setPageTransformer(compositePageTransformer);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                sliderHandler.removeCallbacks(sliderRunnable);
                sliderHandler.postDelayed(sliderRunnable, 2500);  //Slide duration 3 second
            }
        });
    }

//    private void setParentCategoryRecycler(List<ParentList> parentLists){
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
//        homeRecycler.setLayoutManager(layoutManager);
//
//        parentRecyclerAdapter = new ParentRecyclerAdapter(getContext(), parentLists);
//        homeRecycler.setAdapter(parentRecyclerAdapter);
//    }

    private Runnable sliderRunnable = new Runnable() {
        @Override
        public void run() {
            viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        sliderHandler.removeCallbacks(sliderRunnable);
    }

    @Override
    public void onResume() {
        super.onResume();
        sliderHandler.postDelayed(sliderRunnable, 2500);
    }

    public void JSON_DATA_WEB_CALL(String username){
        jsonArrayRequest = new JsonArrayRequest(Globalconstants.URL_PREVIOUTRANS+"&userid="+username,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //progressBar.setVisibility(View.GONE);

                        JSON_PARSE_DATA_AFTER_WEBCALL(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(jsonArrayRequest);
    }

    public void JSON_PARSE_DATA_AFTER_WEBCALL(JSONArray array){

        for(int i = 0; i<array.length(); i++) {
            PreviousTrans GetDataAdapter2 = new PreviousTrans();
            JSONObject json = null;
            try {

                 json = array.getJSONObject(i);

                GetDataAdapter2.setTransDate(json.getString(JSON_FUELDATE));
                GetDataAdapter2.setTransAmount(json.getString(JSON_FUELAMT));
                GetDataAdapter2.setTransLocation(json.getString(JSON_FUELLOC));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            GetDataAdapter1.add(GetDataAdapter2);
        }
        recyclerViewadapter = new PreviousTransAdapter(GetDataAdapter1, getContext());
        homeRecycler.setAdapter(recyclerViewadapter);
    }



}