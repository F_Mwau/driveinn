package com.fmwau.driveinn.DashBoard_and_Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fmwau.driveinn.Adapters.ReportTabAdapter;
import com.fmwau.driveinn.R;
import com.google.android.material.tabs.TabLayout;

public class ReportsFragment extends Fragment {

    TabLayout reportsLayout;
    ViewPager2 reportsPager;
    ReportTabAdapter reportTabAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reports, container, false);
        reportsLayout = view.findViewById(R.id.reports_tabs_layout);
        reportsPager = view.findViewById(R.id.reports_view_pager);

        FragmentManager fm = getFragmentManager();
        reportTabAdapter = new ReportTabAdapter(fm, getLifecycle());
        reportsPager.setAdapter(reportTabAdapter);

        reportsLayout.addTab(reportsLayout.newTab().setText("General"));
        reportsLayout.addTab(reportsLayout.newTab().setText("Refueling"));
        reportsLayout.addTab(reportsLayout.newTab().setText("Service"));

        reportsLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        reportsLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        reportsLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                reportsPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        reportsPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                reportsLayout.selectTab(reportsLayout.getTabAt(position));
            }
        });
        return view;
    }
}