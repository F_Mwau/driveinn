package com.fmwau.driveinn.webservice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.fmwau.driveinn.Registration.Login;
import com.fmwau.driveinn.extra.Global;
import com.fmwau.driveinn.extra.Globalconstants;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by VMutisya on 11/16/2016.
 */
public class Forget_webservice extends AsyncTask<String, Void, String> {
    Context context;
    ProgressDialog pdia;
    String type;
    ArrayList<HashMap<String,String>> array_booking_value;
    Global global;
    public Forget_webservice(Context context) {
        // TODO Auto-generated constructor stub
        this.context=context;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        // global=(Global)context.getApplicationContext();
        pdia = ProgressDialog.show(context, "Loading...", "Please Wait.");

    }


    @Override
    protected String doInBackground(String... params) {
        String response = "";
        try
        {
            String param="&emailAddress=" + URLEncoder.encode(params[0],"UTF-8")+
                    "&auth_key="+ URLEncoder.encode("123","UTF-8");

            Log.e("Activate PARAM",param);

            response= com.fmwau.driveinn.webservice.GetAndPost.postMethodForget(param);
            //System.out.println("Login URL"+response);

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        Log.e("response login", response);
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);


        try {
            if(!result.equalsIgnoreCase("")){
                JSONObject job =new JSONObject(result);

                String status=job.getString("status");

                if (status.equalsIgnoreCase("2")) {
                    pdia.dismiss();
                    array_booking_value=new ArrayList<HashMap<String,String>>();
                    HashMap<String,String> map=new HashMap<String,String>();

                    //String id = job.getString("user_id");

                    SharedPreferences sp = context.getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
                    String login_status           = job.getString("status");
                    SharedPreferences.Editor ed   = sp.edit();
                    //ed.putString(Globalconstants.USER_ID, id);

                    ed.commit();

                    array_booking_value.add(map);

                    if(login_status.equalsIgnoreCase("0")){
                        // amend this to show my booking
                        String message=job.getString("message");
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(context, Login.class);
                        context.startActivity(i);
                        ((Activity)context).finish();
                    }


                }
                else {
                    pdia.dismiss();
                    String message=job.getString("message");
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                }
            }
            else{
                pdia.dismiss();
                Toast.makeText(context, "Network Problem", Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
