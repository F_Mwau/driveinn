package com.fmwau.driveinn.webservice;

import com.fmwau.driveinn.extra.Globalconstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class GetAndPost {




    public static String postMethodLogin(String param) throws IOException {

        URL url = new URL(Globalconstants.URL_LOGIN);

        HttpURLConnection connection = (HttpURLConnection)url.openConnection();

        connection.setRequestMethod("POST");
        // You need to set it to true if you want to send (output) a request body, for example with POST or PUT requests. Sending the request body itself is done via the connection's output stream:
        connection.setDoInput(true);

        connection.setFixedLengthStreamingMode(param.getBytes().length);
        //send the POST out
        PrintWriter out = new PrintWriter(connection.getOutputStream());
        out.print(param);
        out.close();

        String readStream = readStream(connection.getInputStream());

        return readStream;

    }

    public static String postMethodSignUp(String param) throws IOException {

        URL url = new URL(Globalconstants.URL_SINGUP);

        HttpURLConnection connection = (HttpURLConnection)url.openConnection();

        connection.setRequestMethod("POST");
        // You need to set it to true if you want to send (output) a request body, for example with POST or PUT requests. Sending the request body itself is done via the connection's output stream:
        connection.setDoInput(true);

        connection.setFixedLengthStreamingMode(param.getBytes().length);
        //send the POST out
        PrintWriter out = new PrintWriter(connection.getOutputStream());
        out.print(param);
        out.close();

        String readStream = readStream(connection.getInputStream());

        return readStream;

    }



    public static String postMethodForget(String param) throws IOException {

        URL url = new URL(Globalconstants.URL_FORGOT);

        HttpURLConnection connection = (HttpURLConnection)url.openConnection();

        connection.setRequestMethod("POST");
        // You need to set it to true if you want to send (output) a request body, for example with POST or PUT requests. Sending the request body itself is done via the connection's output stream:
        connection.setDoInput(true);

        connection.setFixedLengthStreamingMode(param.getBytes().length);
        //send the POST out
        PrintWriter out = new PrintWriter(connection.getOutputStream());
        out.print(param);
        out.close();

        String readStream = readStream(connection.getInputStream());

        return readStream;

    }

    public static String postMethodRefuel(String param) throws IOException {

        URL url = new URL(Globalconstants.URL_REFUEL);

        HttpURLConnection connection = (HttpURLConnection)url.openConnection();

        connection.setRequestMethod("POST");
        // You need to set it to true if you want to send (output) a request body, for example with POST or PUT requests. Sending the request body itself is done via the connection's output stream:
        connection.setDoInput(true);

        connection.setFixedLengthStreamingMode(param.getBytes().length);
        //send the POST out
        PrintWriter out = new PrintWriter(connection.getOutputStream());
        out.print(param);
        out.close();

        String readStream = readStream(connection.getInputStream());

        return readStream;

    }

    public static String postMethodService(String param) throws IOException {

        URL url = new URL(Globalconstants.URL_SERVICE);

        HttpURLConnection connection = (HttpURLConnection)url.openConnection();

        connection.setRequestMethod("POST");
        // You need to set it to true if you want to send (output) a request body, for example with POST or PUT requests. Sending the request body itself is done via the connection's output stream:
        connection.setDoInput(true);

        connection.setFixedLengthStreamingMode(param.getBytes().length);
        //send the POST out
        PrintWriter out = new PrintWriter(connection.getOutputStream());
        out.print(param);
        out.close();

        String readStream = readStream(connection.getInputStream());

        return readStream;

    }

    public static String postMethodAddvehicle(String param) throws IOException {

        URL url = new URL(Globalconstants.URL_ADDCAR);

        HttpURLConnection connection = (HttpURLConnection)url.openConnection();

        connection.setRequestMethod("POST");
        // You need to set it to true if you want to send (output) a request body, for example with POST or PUT requests. Sending the request body itself is done via the connection's output stream:
        connection.setDoInput(true);

        connection.setFixedLengthStreamingMode(param.getBytes().length);
        //send the POST out
        PrintWriter out = new PrintWriter(connection.getOutputStream());
        out.print(param);
        out.close();

        String readStream = readStream(connection.getInputStream());

        return readStream;

    }



    public static String getMethod(String linkparams) throws IOException {

        URL url = new URL(linkparams);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();

        connection.setRequestMethod("GET");

        String readStream = readStream(connection.getInputStream());

        return  readStream;
    }


    private static String readStream(InputStream in) {
        StringBuilder sb = new StringBuilder();
        try
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String nextLine = "";
            while ((nextLine = reader.readLine()) != null)
            {
                sb.append(nextLine);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return sb.toString();
    }

}
