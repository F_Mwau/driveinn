package com.fmwau.driveinn.webservice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.fmwau.driveinn.DashBoard_and_Fragments.DashBoard;
import com.fmwau.driveinn.extra.Global;
import com.fmwau.driveinn.extra.Globalconstants;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

public class AddCar_webservice extends AsyncTask<String, Void, String> {
    Context context;
    ProgressDialog pdia;
    String type;
    ArrayList<HashMap<String,String>> array_login_value;
    Global global;
    public AddCar_webservice(Context context) {
        // TODO Auto-generated constructor stub
        this.context=context;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        // global=(Global)context.getApplicationContext();
        pdia = ProgressDialog.show(context, "Loading...", "Please Wait.");

    }

    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub
        String response = "";
        try
        {
            String param="&userid=" + URLEncoder.encode(params[0],"UTF-8")+
                    "&regno="+ URLEncoder.encode(params[1],"UTF-8")+
                    "&carmake="+ URLEncoder.encode(params[2],"UTF-8")+
                    "&carmodel="+ URLEncoder.encode(params[3],"UTF-8");

            Log.e("Login PARAM",param);

            response= com.fmwau.driveinn.webservice.GetAndPost.postMethodAddvehicle(param);
            //System.out.println("Login URL"+response);

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        Log.e("response login", response);
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);


        try {
            if(!result.equalsIgnoreCase("")){
                JSONObject jobs =new JSONObject(result);

                String status=jobs.getString("error");

                if (status.equalsIgnoreCase("false")) {
                    pdia.dismiss();


                        Intent i = new Intent(context, DashBoard.class);
                        context.startActivity(i);
                        ((Activity)context).finish();
                }
                else {
                    pdia.dismiss();
                    String message=jobs.getString("message");
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                }
            }
            else{
                pdia.dismiss();
                Toast.makeText(context, "Network Problem", Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
