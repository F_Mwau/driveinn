package com.fmwau.driveinn.webservice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.fmwau.driveinn.DashBoard_and_Fragments.DashBoard;
import com.fmwau.driveinn.extra.Global;
import com.fmwau.driveinn.extra.Globalconstants;

import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

public class Signup_webservice extends AsyncTask<String, Void, String> {
    Context context;
    ProgressDialog pdia;
    String type;
    ArrayList<HashMap<String,String>> array_login_value;
    Global global;

    public Signup_webservice(Context context) {
        // TODO Auto-generated constructor stub
        this.context=context;
    }


    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        // global=(Global)context.getApplicationContext();
        pdia = ProgressDialog.show(context, "Loading...", "Please Wait.");

    }

    @Override
    protected String doInBackground(String... params) {
        String response = "";
        try
        {
            String param="&fullname=" + URLEncoder.encode(params[0],"UTF-8")+
                    "&mobileno="+ URLEncoder.encode(params[1],"UTF-8")+
                    "&email="+ URLEncoder.encode(params[2],"UTF-8")+
                    "&password="+ URLEncoder.encode(params[3],"UTF-8")+
                    "&method="+ URLEncoder.encode("user_signup","UTF-8")+
                    "&auth_key="+ URLEncoder.encode("123","UTF-8");

            Log.e("Sign Up PARAM",param);

            response= com.fmwau.driveinn.webservice.GetAndPost.postMethodSignUp(param);
            //System.out.println("Login URL"+response);

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        Log.e("response login", response);
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);


        try {
            if(!result.equalsIgnoreCase("")){
                JSONObject jobs =new JSONObject(result);
                Log.e("response result", result);
                String status=jobs.getString("error");

                if (status.equalsIgnoreCase("false")) {
                    pdia.dismiss();
                    array_login_value=new ArrayList<HashMap<String,String>>();
                    HashMap<String,String> map=new HashMap<String,String>();
                    JSONObject job =jobs.getJSONObject("user");
                    String id = job.getString("userid");
                    SharedPreferences sp = context.getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
                    String username               = job.getString("email");
                    String mobile_number          = job.getString("mobileno");
                    String login_status           = jobs.getString("error");
                    String email                  = job.getString("email");
                    String customername           = job.getString("fullname");
                    String barcode                = job.getString("barcode");
                    Log.e("barcode result", barcode);
                    SharedPreferences.Editor ed   = sp.edit();

                    ed.putString(Globalconstants.USER_ID, id);
                    ed.putString("userid", id);
                    ed.putString("username", username);
                    ed.putString("email", email);
                    ed.putString("barcode", barcode);
                    ed.putString("customername", customername);
                    //ed.putString("image", image);
                    ed.commit();
                    map.put(Globalconstants.username,username);
                    map.put(Globalconstants.mobile_number,mobile_number);
                    map.put(Globalconstants.Email_id,email);
                    map.put(Globalconstants.login_status,login_status);
                    map.put(Globalconstants.barcode,barcode);
                    map.put(Globalconstants.USER_ID,id);
                    //  map.put(Globalconstants.Email_id,Email_id);
                    array_login_value.add(map);

                    if(login_status.equalsIgnoreCase("false")){

                        Intent i = new Intent(context, DashBoard.class);
                        context.startActivity(i);
                        ((Activity)context).finish();
                    } else {
                        pdia.dismiss();
                        String message=jobs.getString("message");
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }


                }
                else {
                    pdia.dismiss();
                    String message=jobs.getString("message");
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                }
            }
            else{
                pdia.dismiss();
                Toast.makeText(context, "Network Problem", Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
