package com.fmwau.driveinn.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.fmwau.driveinn.ReportFragments.General;
import com.fmwau.driveinn.ReportFragments.Refuel;
import com.fmwau.driveinn.ReportFragments.Service;

public class ReportTabAdapter extends FragmentStateAdapter {

    public ReportTabAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 1 :
                return new Refuel();
            case 2 :
                return new Service();

        }
        return new General();
    }

    @Override
    public int getItemCount() {
        return 3;
    }

}
