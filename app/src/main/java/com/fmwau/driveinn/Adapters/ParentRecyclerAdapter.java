package com.fmwau.driveinn.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fmwau.driveinn.R;
import com.fmwau.driveinn.models.ChildList;
import com.fmwau.driveinn.models.ParentList;

import java.util.List;

public class ParentRecyclerAdapter extends RecyclerView.Adapter<ParentRecyclerAdapter.ParentViewHolder> {

    private Context context;
    private List<ParentList> parentLists;

    public ParentRecyclerAdapter(Context context, List<ParentList> parentLists) {
        this.context = context;
        this.parentLists = parentLists;
    }

    @NonNull
    @Override
    public ParentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ParentViewHolder(LayoutInflater.from(context).inflate(R.layout.parent_list_design, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ParentViewHolder holder, int position) {
        holder.monthTxt.setText(parentLists.get(position).getMonth());
        holder.yearTxt.setText(parentLists.get(position).getYear());
        setChildItemRecycler(holder.childRecyclerView, parentLists.get(position).getChildLists());
    }

    @Override
    public int getItemCount() {
        return parentLists.size();
    }

    public class ParentViewHolder extends RecyclerView.ViewHolder{

        TextView monthTxt, yearTxt;
        RecyclerView childRecyclerView;

        public ParentViewHolder(@NonNull View itemView) {
            super(itemView);

            monthTxt = itemView.findViewById(R.id.month_text);
            yearTxt = itemView.findViewById(R.id.year_text);
            childRecyclerView = itemView.findViewById(R.id.groupRecyclerMember);
        }
    }

    private void setChildItemRecycler(RecyclerView recyclerView, List<ChildList> childLists){
        ChildRecyclerAdapter childRecyclerAdapter = new ChildRecyclerAdapter(context, childLists);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(childRecyclerAdapter);
    }
}
