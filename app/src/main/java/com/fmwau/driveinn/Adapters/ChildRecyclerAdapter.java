package com.fmwau.driveinn.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fmwau.driveinn.R;
import com.fmwau.driveinn.models.ChildList;

import java.util.List;

public class ChildRecyclerAdapter extends RecyclerView.Adapter<ChildRecyclerAdapter.ChildViewHolder> {

    private Context context;
    private List<ChildList> childList;

    public ChildRecyclerAdapter(Context context, List<ChildList> childList) {
        this.context = context;
        this.childList = childList;
    }

    @NonNull
    @Override
    public ChildViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChildViewHolder(LayoutInflater.from(context).inflate(R.layout.child_list_design, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ChildViewHolder holder, int position) {
        holder.dateTxt.setText(childList.get(position).getDate());
        holder.locationTxt.setText(childList.get(position).getLocation());
        holder.amountTxt.setText(childList.get(position).getAmount());
    }

    @Override
    public int getItemCount() {
        return childList.size();
    }

    public class ChildViewHolder extends RecyclerView.ViewHolder {

        TextView dateTxt, locationTxt, amountTxt;

        public ChildViewHolder(@NonNull View itemView) {
            super(itemView);
            dateTxt = itemView.findViewById(R.id.dateTxt);
            locationTxt = itemView.findViewById(R.id.locationTxt);
            amountTxt = itemView.findViewById(R.id.amountTxt);
        }
    }
}
