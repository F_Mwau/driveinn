package com.fmwau.driveinn.extra;

public class SliderUtils {

    String sliderImageUrl;

    public String getSliderImageUrl() {
        return sliderImageUrl;
    }

    public void setSliderImageUrl(String sliderImageUrl) {
        this.sliderImageUrl = sliderImageUrl;
    }
}
