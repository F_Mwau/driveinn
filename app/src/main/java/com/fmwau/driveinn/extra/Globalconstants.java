package com.fmwau.driveinn.extra;

//22136
public class Globalconstants {

    //public static String URL = "http://testondev.com/cdg/victor/userServices/index";
    //public static String URL = "http://www.ghhltd.com/greyhoundcabs/android_login_api/android_login_api/";

    public static String URL_LOGIN              = "http://45.33.62.76/petro/v1/api.php?apicall=userLogin";
    public static String URL_SINGUP             = "http://45.33.62.76/petro/v1/api.php?apicall=createuser";
    public static String URL_FORGOT             = "http://45.33.62.76/petro/v1/api.php?apicall=forgetpass.php";
    public static String URL_PREVIOUTRANS       = "http://45.33.62.76/petro/v1/api.php?apicall=getPreviousFuel";
    public static String URL_REFUEL             = "http://45.33.62.76/petro/v1/api.php?apicall=addFuel";
    public static String URL_SERVICE            = "http://45.33.62.76/petro/v1/api.php?apicall=addService";
    public static String URL_ADDCAR             = "http://45.33.62.76/petro/v1/api.php?apicall=addvehicle";
    public static String URL_FUELTRANS          = "http://45.33.62.76/petro/v1/api.php?apicall=getPeriodicperUserFuel";
    public static String URL_FUELHEADER          = "http://45.33.62.76/petro/v1/api.php?apicall=getPeriodicFuel";
    public static String URL_USERCARS           = "http://45.33.62.76/petro/v1/api.php?apicall=getUserCars";
    public static String URL_SERVICEPERIOD      = "http://45.33.62.76/petro/v1/api.php?apicall=getPeriodicService";
    public static String URL_SERVICERANS        = "http://45.33.62.76/petro/v1/api.php?apicall=getPeriodicperUserService";
    public static String OTP_VERRIFY            = "http://45.33.62.76/petro/v1/api.php?apicall=otpverify";
    public static String URL_BANNERS           = "http://45.33.62.76/petro/v1/api.php?apicall=getBanners";

    public static String USER_ID                = "USER_ID";
    public static String pref                   = "com.app.victor.driver";
    //---------------------------for device id---------------------------------

    public static String device_id                = "";
    public static final String REG_NO             = "regno";
    public static final String JSON_ARRAY         = "results";
    public static final String DEVICE_ID          = "sghh";
    public static final String DEVICE_STATUS      = "fsf";
    public static final String GCM_SENDER_ID      = "590638466132";
    public static String GCM_APPLICATION_DEVICEID = "";

    //------------------------Exit--------------------------------------------

    //----------------for login------------------------------
    public static String username               = "username";
    public static String mobile_number          = "mobile_number";
    public static String image                  = "image";
    public static String login_status           = "login_status";
    public static String changepassword_status  = "changepassword_status";
    public static String Email_id               = "Email_id";
    public static String barcode             = "barcode";
    //------------------login exit---------------------------

    //------------------for profile----------------------------------
    public static String first_name   = "first_name";
    public static String last_name    = "last_name";
    public static String mobilenumber = "mobilenumber";
    public static String user_image   = "user_image";
    public static String email        = "email";



    //----------------------profile exit-------------------------------

    //--------------------Check_Driver-----------------------------------
    public static String pick_location    = "pick_location";
    public static String drop_location    = "drop_location";
    public static String request_id       = "request_id";
    public static String time             = "time";
    public static String date             = "date";
    public static String driver_latitude  = "driver_latitude";
    public static String driver_longitude = "driver_longitude";
    public static String drivername       = "drivername";
    public static String phonenumber      = "phonenumber";
    public static String plate_number     = "plate_number";
    public static String request_status   = "request_status";
    public static String latitude         = "latitude";
    public static String longitude        = "longitude";
    public static String driver_id        = "driver_id";

    //---------------------Exit-----------------------------------------------
    //-----------------For booking history-------------------------------------------

    public static String pick_location_book = "pick_location_book";
    public static String drop_location_book = "drop_location_book";
    public static String drivername_book    = "drivername_book";
    public static String complete_status    = "complete_status";


    //-------------------Exit----------------------------------------------------------
}
