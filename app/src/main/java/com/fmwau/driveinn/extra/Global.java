package com.fmwau.driveinn.extra;

import android.app.Application;
import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;


public class Global extends Application {

    ArrayList<HashMap<String,String>> array_profile=new ArrayList<HashMap<String,String>>();

    public ArrayList<HashMap<String, String>> getArray_map_new() {
        return array_map_new;
    }

    public void setArray_map_new(ArrayList<HashMap<String, String>> array_map_new) {
        this.array_map_new = array_map_new;
    }

    ArrayList<HashMap<String,String>> array_map_new=new ArrayList<HashMap<String,String>>();

    public ArrayList<HashMap<String, String>> getBookhistory_new() {
        return bookhistory_new;
    }

    public void setBookhistory_new(ArrayList<HashMap<String, String>> bookhistory_new) {
        this.bookhistory_new = bookhistory_new;
    }

    ArrayList<HashMap<String,String>> bookhistory_new=new ArrayList<HashMap<String,String>>();
    public ArrayList<HashMap<String, String>> getCheck_driver_new() {
        return check_driver_new;
    }

    public void setCheck_driver_new(ArrayList<HashMap<String, String>> check_driver_new) {
        this.check_driver_new = check_driver_new;
    }

    ArrayList<HashMap<String,String>> check_driver_new=new ArrayList<HashMap<String,String>>();
    public ArrayList<HashMap<String, String>> getArray_profile() {
        return array_profile;
    }

    public void setArray_profile(ArrayList<HashMap<String, String>> array_profile) {
        this.array_profile = array_profile;
    }

    ArrayList<String> location=new ArrayList<String>();
    ArrayList<HashMap<String,String>> array_login_profile=new ArrayList<HashMap<String,String>>();
    public ArrayList<String> getLocation() {
        return location;
    }

    public void setLocation(ArrayList<String> location) {
        this.location = location;
    }

    public ArrayList<HashMap<String, String>> getArray_login_profile() {
        return array_login_profile;
    }

    public void setArray_login_profile(ArrayList<HashMap<String, String>> array_login_profile) {
        this.array_login_profile = array_login_profile;
    }
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //MultiDex.install(this);
    }
}
