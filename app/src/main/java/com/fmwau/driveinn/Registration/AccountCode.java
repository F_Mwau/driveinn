package com.fmwau.driveinn.Registration;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.fmwau.driveinn.DashBoard_and_Fragments.DashBoard;
import com.fmwau.driveinn.R;

public class AccountCode extends AppCompatActivity {

    EditText codeTxt;
    Button accountBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_code);
        hooks();
    }

    private void hooks() {
        codeTxt = findViewById(R.id.accountCodeTxt);
        accountBtn = findViewById(R.id.accountCodeBtn);

        accountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent corporate = new Intent(getApplicationContext(), DashBoard.class);
                startActivity(corporate);
            }
        });
    }
}