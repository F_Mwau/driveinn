package com.fmwau.driveinn.Registration;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fmwau.driveinn.DashBoard_and_Fragments.DashBoard;
import com.fmwau.driveinn.R;
import com.fmwau.driveinn.webservice.Login_webservice;

public class Login extends AppCompatActivity {

    EditText emailTxt, passwordTxt;
    TextView signUp, forgotPassword;
    Button logInBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        hooks();
    }

    private void hooks() {
        signUp = findViewById(R.id.signUpTxt);
        forgotPassword = findViewById(R.id.forgotPasswordTxt);
        logInBtn = findViewById(R.id.loginBtn);
        passwordTxt = findViewById(R.id.loginPasswordEditTxt);
        emailTxt = findViewById(R.id.loginEmailEditTxt);


        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signUp = new Intent(getApplicationContext(), Accounts.class);
                startActivity(signUp);
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent forgotPass = new Intent(getApplicationContext(), ForgotPassword.class);
                startActivity(forgotPass);
            }
        });

        logInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = emailTxt.getText().toString();
                String password = passwordTxt.getText().toString();
                if(email.trim().length() ==0){
                    Toast.makeText(Login.this, "Username is required", Toast.LENGTH_LONG).show();
                }else if(password.trim().length() ==0 ){
                    Toast.makeText(Login.this, "Username is required", Toast.LENGTH_LONG).show();
                }else {
                    Login_webservice  login = new Login_webservice(Login.this);
                    login.execute(email,password);
                }
            }
        });
    }
}