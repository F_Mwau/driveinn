package com.fmwau.driveinn.Registration;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.fmwau.driveinn.R;

public class Accounts extends AppCompatActivity {

    CardView corporateBtn, personalBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts);
        hooks();
    }

    private void hooks() {
        corporateBtn = findViewById(R.id.corporateCardBtn);
        personalBtn = findViewById(R.id.personalCardBtn);

        personalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signUp = new Intent(getApplicationContext(), Signup.class);
                startActivity(signUp);
            }
        });

        corporateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent corporate = new Intent(getApplicationContext(), AccountCode.class);
                startActivity(corporate);
            }
        });
    }
}