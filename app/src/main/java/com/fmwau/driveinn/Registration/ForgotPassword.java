package com.fmwau.driveinn.Registration;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.fmwau.driveinn.DashBoard_and_Fragments.DashBoard;
import com.fmwau.driveinn.R;

public class ForgotPassword extends AppCompatActivity {

    Button nextBtn;
    EditText emailTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        hooks();
    }

    private void hooks() {
        nextBtn = findViewById(R.id.forgotNxtBtn);
        emailTxt = findViewById(R.id.forgotEmailEditTxt);

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent forgotPass = new Intent(getApplicationContext(), DashBoard.class);
                startActivity(forgotPass);
            }
        });
    }
}