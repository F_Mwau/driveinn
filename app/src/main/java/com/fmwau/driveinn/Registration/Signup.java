package com.fmwau.driveinn.Registration;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fmwau.driveinn.DashBoard_and_Fragments.DashBoard;
import com.fmwau.driveinn.R;
import com.fmwau.driveinn.webservice.Signup_webservice;

public class Signup extends AppCompatActivity {

    EditText fullNameTxt, phoneTxt, emailTxt, passwordTxt;
    TextView loginTxt;
    Button signUpBtn;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        hooks();
    }

    private void hooks() {
        fullNameTxt = findViewById(R.id.signUpFullNameTxt);
        phoneTxt = findViewById(R.id.signUpPhoneNumberTxt);
        emailTxt = findViewById(R.id.signUpEmailTxt);
        passwordTxt = findViewById(R.id.signUpPasswordTxt);
        loginTxt = findViewById(R.id.logInTxt);
        signUpBtn = findViewById(R.id.signupBtn);

        loginTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent logIn = new Intent(getApplicationContext(), Login.class);
                startActivity(logIn);
            }
        });

        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String fullname = fullNameTxt.getText().toString();
                String mobile = phoneTxt.getText().toString();
                String email = emailTxt.getText().toString();
                String password = passwordTxt.getText().toString();
                if (fullname.trim().length()  ==0) {
                    Toast.makeText(Signup.this, "Full Name is required", Toast.LENGTH_LONG).show();
                } else if (mobile.trim().length() == 0) {
                    Toast.makeText(Signup.this, "Mobile is required", Toast.LENGTH_LONG).show();
                } else if (password.trim().length() == 0) {
                    Toast.makeText(Signup.this, "Password is required", Toast.LENGTH_LONG).show();
                }else if (email.trim().length() == 0) {
                    Toast.makeText(Signup.this, "Email No is required", Toast.LENGTH_LONG).show();
                }else {
                    Signup_webservice signup = new Signup_webservice(Signup.this);
                    signup.execute(fullname,mobile,email,password);

                }
            }
        });
    }
}