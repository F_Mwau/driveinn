package com.fmwau.driveinn.ReportFragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fmwau.driveinn.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class General extends Fragment {

    BarChart refuelChart, serviceChart;
    PieChart generalChart;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_general, container, false);
        refuelChart = view.findViewById(R.id.refuelReportsChart);
        serviceChart = view.findViewById(R.id.servicingReportsChart);
        generalChart = view.findViewById(R.id.generalPieChart);

        ArrayList<BarEntry> refuel = new ArrayList<>();
        refuel.add(new BarEntry(1, 500));
        refuel.add(new BarEntry(2, 900));
        refuel.add(new BarEntry(3, 436));
        refuel.add(new BarEntry(4, 800));
        refuel.add(new BarEntry(5, 380));
        refuel.add(new BarEntry(6, 510));
        refuel.add(new BarEntry(7, 456));
        refuel.add(new BarEntry(8, 431));

        ArrayList<BarEntry> servicing = new ArrayList<>();
        servicing.add(new BarEntry(1, 500));
        servicing.add(new BarEntry(2, 900));
        servicing.add(new BarEntry(3, 436));
        servicing.add(new BarEntry(4, 800));
        servicing.add(new BarEntry(5, 380));
        servicing.add(new BarEntry(6, 510));
        servicing.add(new BarEntry(7, 456));
        servicing.add(new BarEntry(8, 431));

        ArrayList<PieEntry> general = new ArrayList<>();
        general.add(new PieEntry(894, "Refuel"));
        general.add(new PieEntry(291, "Service"));

        BarDataSet refuelDataSet = new BarDataSet(refuel, "Refuel Chart");
        refuelDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        refuelDataSet.setValueTextColor(Color.BLACK);
        refuelDataSet.setValueTextSize(16f);
        BarData refuelData = new BarData(refuelDataSet);
        refuelChart.setFitBars(true);
        refuelChart.setData(refuelData);
        refuelChart.getDescription().setText("Refueling");
        refuelChart.animateY(2000);

        BarDataSet serviceDataSet = new BarDataSet(servicing, "Servicing Chart");
        serviceDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        serviceDataSet.setValueTextColor(Color.BLACK);
        serviceDataSet.setValueTextSize(18f);
        BarData serviceData = new BarData(serviceDataSet);
        serviceChart.setFitBars(true);
        serviceChart.setData(serviceData);
        serviceChart.getDescription().setText("Servicing");
        serviceChart.animateY(2000);

        PieDataSet generalDataSet = new PieDataSet(general, "General");
        generalDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        generalDataSet.setValueTextColor(Color.BLACK);
        generalDataSet.setValueTextSize(18f);
        PieData generalData = new PieData(generalDataSet);
        generalChart.setData(generalData);
        generalChart.getDescription().setEnabled(false);
        generalChart.setCenterText("General Chart");
        generalChart.animate();



        return view;
    }
}