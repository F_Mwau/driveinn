package com.fmwau.driveinn.ReportFragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.fmwau.driveinn.R;
import com.fmwau.driveinn.dbadapters.FuelTrans;
import com.fmwau.driveinn.dbadapters.FuelTransAdapater;
import com.fmwau.driveinn.extra.Globalconstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RefuleActivity extends AppCompatActivity {

    private RecyclerView homeRecycler;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;
    JsonArrayRequest jsonArrayRequest ;
    RequestQueue requestQueue ;
    List<FuelTrans> GetDataAdapter1;

    String JSON_FUEDATE= "fueldate";
    String JSON_FUELAMT= "fuelamount";
    String JSON_FUELLOC= "fuellocation";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refule);
        GetDataAdapter1 = new ArrayList<>();
        homeRecycler = findViewById(R.id.fuelRecyclerTrans);
        homeRecycler.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(this);
        homeRecycler.setLayoutManager(recyclerViewlayoutManager);
        SharedPreferences sharedPreferences = this.getSharedPreferences(Globalconstants.pref, MODE_PRIVATE);
        String  requestId = sharedPreferences.getString("requestID", "");
        String [] dataSplit = requestId.split("-");
        String userId = sharedPreferences.getString("userId", "");
        System.out.println("month===>>>>"+dataSplit[0]+" Year ======"+dataSplit[1]+" userId==============>"+userId);
        JSON_DATA_WEB_CALL(sharedPreferences.getString("userid",""),dataSplit[0],dataSplit[1]);
    }

    public void JSON_DATA_WEB_CALL(String username,String month, String year){
        jsonArrayRequest = new JsonArrayRequest(Globalconstants.URL_FUELTRANS+"&userid="+username+"&month="+month+"&year="+year,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //progressBar.setVisibility(View.GONE);

                        JSON_PARSE_DATA_AFTER_WEBCALL(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }


    public void JSON_PARSE_DATA_AFTER_WEBCALL(JSONArray array){
        for(int i = 0; i<array.length(); i++) {
            FuelTrans GetDataAdapter2 = new FuelTrans();
            JSONObject json = null;
            try {

                json = array.getJSONObject(i);

                GetDataAdapter2.setFueltransLocation(json.getString(JSON_FUELLOC));
                GetDataAdapter2.setFueltransDate(json.getString(JSON_FUEDATE));
                GetDataAdapter2.setFueltransAmount(json.getString(JSON_FUELAMT));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            GetDataAdapter1.add(GetDataAdapter2);
        }
        recyclerViewadapter = new FuelTransAdapater(GetDataAdapter1, this);
        homeRecycler.setAdapter(recyclerViewadapter);
    }
}