package com.fmwau.driveinn.ReportFragments;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.fmwau.driveinn.R;
import com.fmwau.driveinn.Task.RefuelActivity;
import com.fmwau.driveinn.dbadapters.FuelPeriodAdapter;
import com.fmwau.driveinn.dbadapters.FuelPeriodTrans;
import com.fmwau.driveinn.dbadapters.FuelTrans;
import com.fmwau.driveinn.dbadapters.FuelTransAdapater;
import com.fmwau.driveinn.extra.Globalconstants;
import com.fmwau.driveinn.models.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class RefuelTrans extends Fragment {

    private RecyclerView homeRecycler;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;
    JsonArrayRequest jsonArrayRequest ;
    RequestQueue requestQueue ;
    List<FuelTrans> GetDataAdapter1;

    String JSON_FUEDATE= "month";
    String JSON_FUELAMT= "year";
    String JSON_FUELLOC= "transcount";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_refuel_trans, container, false);
        GetDataAdapter1 = new ArrayList<>();
        homeRecycler = view.findViewById(R.id.fuelRecyclerTrans);

        homeRecycler.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(view.getContext());
        homeRecycler.setLayoutManager(recyclerViewlayoutManager);
        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(Globalconstants.pref, MODE_PRIVATE);
        String  requestId = sharedPreferences.getString("requestID", "");
        String userId = sharedPreferences.getString("userId", "");
        System.out.println("requestId===>>>>"+requestId+" userId==============>"+userId);


        //JSON_DATA_WEB_CALL(sharedPreferences.getString("userid",""));

        return view;
    }


    public void JSON_DATA_WEB_CALL(String username,String monthyear){
        jsonArrayRequest = new JsonArrayRequest(Globalconstants.URL_FUELTRANS+"&userid="+username,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //progressBar.setVisibility(View.GONE);

                        JSON_PARSE_DATA_AFTER_WEBCALL(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(jsonArrayRequest);
    }


    public void JSON_PARSE_DATA_AFTER_WEBCALL(JSONArray array){
        for(int i = 0; i<array.length(); i++) {
            FuelTrans GetDataAdapter2 = new FuelTrans();
            JSONObject json = null;
            try {

                json = array.getJSONObject(i);

                GetDataAdapter2.setFueltransLocation(json.getString(JSON_FUELLOC));
                GetDataAdapter2.setFueltransDate(json.getString(JSON_FUEDATE));
                GetDataAdapter2.setFueltransAmount(json.getString(JSON_FUELAMT));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            GetDataAdapter1.add(GetDataAdapter2);
        }
        recyclerViewadapter = new FuelTransAdapater(GetDataAdapter1, getContext());
        homeRecycler.setAdapter(recyclerViewadapter);
    }
}