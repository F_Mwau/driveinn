package com.fmwau.driveinn.ReportFragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.fmwau.driveinn.R;
import com.fmwau.driveinn.dbadapters.FuelTrans;
import com.fmwau.driveinn.dbadapters.FuelTransAdapater;
import com.fmwau.driveinn.dbadapters.ServiceTrans;
import com.fmwau.driveinn.dbadapters.ServiceTransAdapter;
import com.fmwau.driveinn.extra.Globalconstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ServiceTransActivity extends AppCompatActivity {

    private RecyclerView homeRecycler;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;
    JsonArrayRequest jsonArrayRequest ;
    RequestQueue requestQueue ;
    List<ServiceTrans> GetDataAdapter1;

    String JSON_SERVICEDATE= "servicedate";
    String JSON_SERVICELOC= "servicelocation";
    String JSON_SERVICEDESC= "servicedesc";
    String JSON_SERVICEAMT ="serviceamount";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service2);
        GetDataAdapter1 = new ArrayList<>();
        homeRecycler = findViewById(R.id.serviceRecyclerTrans);
        homeRecycler.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(this);
        homeRecycler.setLayoutManager(recyclerViewlayoutManager);
        SharedPreferences sharedPreferences = this.getSharedPreferences(Globalconstants.pref, MODE_PRIVATE);
        String  requestId = sharedPreferences.getString("requestID", "");
        String [] dataSplit = requestId.split("-");
        String userId = sharedPreferences.getString("userId", "");
        JSON_DATA_WEB_CALL(sharedPreferences.getString("userid",""),dataSplit[0],dataSplit[1]);
    }

    public void JSON_DATA_WEB_CALL(String username,String month, String year){
        jsonArrayRequest = new JsonArrayRequest(Globalconstants.URL_SERVICERANS+"&userid="+username+"&month="+month+"&year="+year,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //progressBar.setVisibility(View.GONE);

                        JSON_PARSE_DATA_AFTER_WEBCALL(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }


    public void JSON_PARSE_DATA_AFTER_WEBCALL(JSONArray array){
        for(int i = 0; i<array.length(); i++) {
            ServiceTrans GetDataAdapter2 = new ServiceTrans();
            JSONObject json = null;
            try {

                json = array.getJSONObject(i);

                GetDataAdapter2.setServiceDesc(json.getString(JSON_SERVICEDESC));
                GetDataAdapter2.setServceAmount(json.getString(JSON_SERVICEAMT));
                GetDataAdapter2.setServiceLocation(json.getString(JSON_SERVICELOC));
                GetDataAdapter2.setServicetransDate(json.getString(JSON_SERVICEDATE));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            GetDataAdapter1.add(GetDataAdapter2);
        }
        recyclerViewadapter = new ServiceTransAdapter(GetDataAdapter1, this);
        homeRecycler.setAdapter(recyclerViewadapter);
    }
}