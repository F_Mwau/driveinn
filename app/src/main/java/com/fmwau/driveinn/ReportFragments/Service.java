package com.fmwau.driveinn.ReportFragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.fmwau.driveinn.Adapters.ParentRecyclerAdapter;
import com.fmwau.driveinn.R;
import com.fmwau.driveinn.dbadapters.FuelPeriodAdapter;
import com.fmwau.driveinn.dbadapters.FuelPeriodTrans;
import com.fmwau.driveinn.dbadapters.ServicePeriodAdapter;
import com.fmwau.driveinn.dbadapters.ServicePeriodTrans;
import com.fmwau.driveinn.extra.Globalconstants;
import com.fmwau.driveinn.models.ChildList;
import com.fmwau.driveinn.models.ParentList;
import com.fmwau.driveinn.models.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Service extends Fragment {
    private RecyclerView homeRecycler;
    List<ServicePeriodTrans> GetDataAdapter1;

    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;
    ProgressBar progressBar;

    JsonArrayRequest jsonArrayRequest ;
    RequestQueue requestQueue ;

    String JSON_FUELMonth= "month";
    String JSON_FUELYear= "year";
    String JSON_FUELCOUNT= "transcount";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        GetDataAdapter1 = new ArrayList<>();
        homeRecycler = view.findViewById(R.id.serviceRecyclerGroup);
        homeRecycler.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(view.getContext());
        homeRecycler.setLayoutManager(recyclerViewlayoutManager);

        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
        homeRecycler.addOnItemTouchListener(new RecyclerTouchListener(getContext(), homeRecycler, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                Intent intent = new Intent(getContext(), ServiceTransActivity.class);
                String requestID    = GetDataAdapter1.get(position).getServicePeriod();
                String userId = sharedPreferences.getString("userid","");
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("requestID", requestID);
                editor.putString("userId",userId);
                editor.commit();
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        JSON_DATA_WEB_CALL(sharedPreferences.getString("userid",""));
        return view;
    }

    public void JSON_DATA_WEB_CALL(String username){
        jsonArrayRequest = new JsonArrayRequest(Globalconstants.URL_SERVICEPERIOD+"&userid="+username,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //progressBar.setVisibility(View.GONE);

                        JSON_PARSE_DATA_AFTER_WEBCALL(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(jsonArrayRequest);
    }

    public void JSON_PARSE_DATA_AFTER_WEBCALL(JSONArray array){
        for(int i = 0; i<array.length(); i++) {
            ServicePeriodTrans GetDataAdapter2 = new ServicePeriodTrans();
            JSONObject json = null;
            try {

                json = array.getJSONObject(i);

                GetDataAdapter2.setServicePeriod(json.getString(JSON_FUELMonth)+"-"+json.getString(JSON_FUELYear));
                GetDataAdapter2.setServicetranscount(json.getString(JSON_FUELCOUNT));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            GetDataAdapter1.add(GetDataAdapter2);
        }
        recyclerViewadapter = new ServicePeriodAdapter(GetDataAdapter1, getContext());
        homeRecycler.setAdapter(recyclerViewadapter);
    }
}