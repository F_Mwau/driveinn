package com.fmwau.driveinn.ReportFragments;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.fmwau.driveinn.Adapters.ParentRecyclerAdapter;
import com.fmwau.driveinn.R;
import com.fmwau.driveinn.Task.RefuelActivity;
import com.fmwau.driveinn.dbadapters.FuelPeriodAdapter;
import com.fmwau.driveinn.dbadapters.FuelPeriodTrans;
import com.fmwau.driveinn.dbadapters.FuelTrans;
import com.fmwau.driveinn.dbadapters.FuelTransAdapater;
import com.fmwau.driveinn.dbadapters.PreviousTrans;
import com.fmwau.driveinn.dbadapters.PreviousTransAdapter;
import com.fmwau.driveinn.extra.Globalconstants;
import com.fmwau.driveinn.models.RecyclerTouchListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Refuel extends Fragment {

    private RecyclerView homeRecycler;
    List<FuelPeriodTrans> GetDataAdapter1;

    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;
    ProgressBar progressBar;

    JsonArrayRequest jsonArrayRequest ;
    RequestQueue requestQueue ;

    String JSON_FUELMonth= "month";
    String JSON_FUELYear= "year";
    String JSON_FUELCOUNT= "transcount";



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_refuel, container, false);

        GetDataAdapter1 = new ArrayList<>();
        homeRecycler = view.findViewById(R.id.fuelRecyclerGroup);
        homeRecycler.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(view.getContext());
        homeRecycler.setLayoutManager(recyclerViewlayoutManager);

        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
        homeRecycler.addOnItemTouchListener(new RecyclerTouchListener(getContext(), homeRecycler, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                FuelPeriodTrans cdata = GetDataAdapter1.get(position);
                Intent intent = new Intent(getContext(), RefuleActivity.class);
                Bundle bundle = new Bundle();
                String requestID    = GetDataAdapter1.get(position).getFuelPeriod();
                String userId = sharedPreferences.getString("userid","");
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("requestID", requestID);
                editor.putString("userId",userId);
                editor.commit();
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        JSON_DATA_WEB_CALL(sharedPreferences.getString("userid",""));
        return view;
    }


    public void JSON_DATA_WEB_CALL(String username){
        jsonArrayRequest = new JsonArrayRequest(Globalconstants.URL_FUELHEADER+"&userid="+username,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //progressBar.setVisibility(View.GONE);

                        JSON_PARSE_DATA_AFTER_WEBCALL(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(jsonArrayRequest);
    }


    public void JSON_PARSE_DATA_AFTER_WEBCALL(JSONArray array){
        for(int i = 0; i<array.length(); i++) {
            FuelPeriodTrans GetDataAdapter2 = new FuelPeriodTrans();
            JSONObject json = null;
            try {

                json = array.getJSONObject(i);

                GetDataAdapter2.setFuelPeriod(json.getString(JSON_FUELMonth)+"-"+json.getString(JSON_FUELYear));
                GetDataAdapter2.setTranscount(json.getString(JSON_FUELCOUNT));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            GetDataAdapter1.add(GetDataAdapter2);
        }
        recyclerViewadapter = new FuelPeriodAdapter(GetDataAdapter1, getContext());
        homeRecycler.setAdapter(recyclerViewadapter);
    }





}