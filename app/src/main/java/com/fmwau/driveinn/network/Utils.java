package com.fmwau.driveinn.network;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Utils {

    private static final String DIRECTION_API = "https://maps.googleapis.com/maps/api/directions/json?origin=";
    public static final String API_KEY = "DIRECTION_API_KEY";

    public static final int MY_SOCKET_TIMEOUT_MS = 5000;

    public static String getUrlSit(String startingAddress, String destinationAddress){
        return DIRECTION_API +startingAddress+"&destination="+destinationAddress+"&key="+API_KEY;
    }

    public static String getUrl(String originLat, String originLon, String destinationLat, String destinationLon){
        return Utils.DIRECTION_API + originLat+","+originLon+"&destination="+destinationLat+","+destinationLon+"&key="+API_KEY;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
