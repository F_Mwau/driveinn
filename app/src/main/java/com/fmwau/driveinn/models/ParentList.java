package com.fmwau.driveinn.models;

import java.util.List;

public class ParentList {

    String month, year;
    List<ChildList> childLists;

    public ParentList(String month, String year, List<ChildList> childLists) {
        this.month = month;
        this.year = year;
        this.childLists = childLists;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<ChildList> getChildLists() {
        return childLists;
    }

    public void setChildLists(List<ChildList> childLists) {
        this.childLists = childLists;
    }
}
