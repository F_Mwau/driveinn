package com.fmwau.driveinn.models;

public class ChildList {
    String date, location, amount;


    public ChildList(String date, String location, String amount) {
        this.date = date;
        this.location = location;
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
