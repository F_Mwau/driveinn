package com.fmwau.driveinn.Task;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.fmwau.driveinn.DashBoard_and_Fragments.DashBoard;
import com.fmwau.driveinn.R;
import com.fmwau.driveinn.extra.Globalconstants;
import com.fmwau.driveinn.webservice.AddCar_webservice;

public class AddCarActivity extends AppCompatActivity {

    ImageView backBtn;
    EditText car_manu,car_model,car_regno;
    Button carDetailsBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);
        hooks();
    }

    private void hooks() {
        backBtn = findViewById(R.id.carRegBack);
        car_manu = findViewById(R.id.car_manu);
        car_model = findViewById(R.id.car_model);
        car_regno = findViewById(R.id.car_regno);
        carDetailsBtn = findViewById(R.id.carDetailsBtn);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent back = new Intent(getApplicationContext(), DashBoard.class);
                startActivity(back);
            }
        });
        carDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
                String userid = sharedPreferences.getString("userid", "");
                String carmanu = car_manu.getText().toString();
                String carmodel = car_model.getText().toString();
                String carRegno = car_regno.getText().toString();
                if(carmodel.trim().length() == 0){
                    Toast.makeText(AddCarActivity.this, "Car Model is required", Toast.LENGTH_LONG).show();
                }else if(carmanu.trim().length() == 0){
                    Toast.makeText(AddCarActivity.this, "Car Manufacture is required", Toast.LENGTH_LONG).show();
                }else if(carRegno.trim().length() == 0){
                    Toast.makeText(AddCarActivity.this, "Car Registration No is required", Toast.LENGTH_LONG).show();
                }else{
                    AddCar_webservice addcar = new AddCar_webservice(AddCarActivity.this);
                    addcar.execute(userid,carRegno,carmanu,carmodel);
                    }
                }
        });
    }
}