package com.fmwau.driveinn.Task;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fmwau.driveinn.DashBoard_and_Fragments.DashBoard;
import com.fmwau.driveinn.R;
import com.fmwau.driveinn.extra.Globalconstants;
import com.fmwau.driveinn.webservice.Service_webserice;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class ServiceActivity extends AppCompatActivity {

    //TextView carSelect;
    private Spinner carservicespinner;
    ArrayList<String> carList;
    EditText dateTxt, amtTxt, serviceTxt, locationTxt;
    Button servicingBtn;
    private int mDate, mMonth, mYear;
    Dialog carDialog;
    ImageView backBtn;

    private ArrayList<String> cars;
    private JSONArray result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        cars = new ArrayList<String>();
        hooks();
        SharedPreferences sharedPreferences = getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
        String userid = sharedPreferences.getString("userid", "");
        getData(userid);

    }

    private void hooks() {
        dateTxt = findViewById(R.id.car_servicing_date_text);
        carservicespinner = findViewById(R.id.service_select_car);
        amtTxt = findViewById(R.id.car_servicing_amt_text);
        serviceTxt = findViewById(R.id.car_servicing_service_text);
        locationTxt = findViewById(R.id.car_servicing_location_text);
        servicingBtn = findViewById(R.id.carServicingBtn);
//        backBtn = findViewById(R.id.serviceBack);



        carList = new ArrayList<>();
        carList.add("KAB 231P");
        carList.add("KCA 321M");
        carList.add("KBM 731S");
        carList.add("KDC 984T");
        carList.add("KDD 738W");

//        carSelect.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                carDialog = new Dialog(ServiceActivity.this);
//                carDialog.setContentView(R.layout.dialog_searchable_spinner);
//                carDialog.getWindow().setLayout(650, 800);
//                carDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//                carDialog.show();
//
//                EditText carRegEditText = carDialog.findViewById(R.id.car_registration_edit_text);
//                ListView carLists = carDialog.findViewById(R.id.carListView);
//                TextView carSelect = findViewById(R.id.service_select_car);
//                ArrayAdapter<String> adapter = new ArrayAdapter<>(ServiceActivity.this, android.R.layout.simple_list_item_1,carList);
//                carLists.setAdapter(adapter);
//
//                carRegEditText.addTextChangedListener(new TextWatcher() {
//                    @Override
//                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                    }
//
//                    @Override
//                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
////                        filter arraylist
//                        adapter.getFilter().filter(charSequence);
//                    }
//
//                    @Override
//                    public void afterTextChanged(Editable editable) {
//
//                    }
//                });
//                carLists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
////                        Set select item on the textView
//                        carSelect.setText(adapter.getItem(i));
//                        carDialog.dismiss();
//                    }
//                });
//            }
//        });

//        backBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent back = new Intent(getApplicationContext(), DashBoard.class);
//                startActivity(back);
//            }
//        });

        dateTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                mDate = calendar.get(Calendar.DATE);
                mMonth = calendar.get(Calendar.MONTH);
                mYear = calendar.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(ServiceActivity.this, android.R.style.Theme_DeviceDefault_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int date) {
                        dateTxt.setText(date + "/" + (month +1)+ "/" + year);

                    }
                }, mYear, mMonth ,mDate);
                datePickerDialog.show();
            }
        });

        Places.initialize(getApplicationContext(),"AIzaSyD0ElZQJP_SxsLjZr3BeSm9sFyMFAneRqQ");
        locationTxt.setFocusable(false);
        locationTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Place.Field> fieldList = Arrays.asList(
                        Place.Field.ADDRESS,
                        Place.Field.LAT_LNG,
                        Place.Field.NAME);
                Intent autoComplete = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fieldList).build(getApplicationContext());
                startActivityForResult(autoComplete, 100);
            }
        });

        servicingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences(Globalconstants.pref, Context.MODE_PRIVATE);
                String userid = sharedPreferences.getString("userid", "");
                String serviceDate = dateTxt.getText().toString();
                String amt = amtTxt.getText().toString();
                String regNo = carservicespinner.getSelectedItem().toString();
                String service = serviceTxt.getText().toString();
                String location = locationTxt.getText().toString();
                if(regNo.trim().length() == 0){
                    Toast.makeText(ServiceActivity.this, "Registration No is required", Toast.LENGTH_LONG).show();
                }else if(serviceDate.trim().length() == 0){
                    Toast.makeText(ServiceActivity.this, "Service Date is required", Toast.LENGTH_LONG).show();
                }else if(service.trim().length() == 0){
                    Toast.makeText(ServiceActivity.this, "Service done is required", Toast.LENGTH_LONG).show();
                }else if(location.trim().length() == 0){
                    Toast.makeText(ServiceActivity.this, "Service location is required", Toast.LENGTH_LONG).show();
                }else {
                    Service_webserice services = new Service_webserice(ServiceActivity.this);
                    services.execute(userid,regNo,service,amt,location,serviceDate);

                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK){
//            On Success Initialize Place and setText on the editText
            Place place = Autocomplete.getPlaceFromIntent(data);
            locationTxt.setText(place.getName());
        } else if(resultCode == AutocompleteActivity.RESULT_ERROR){
//            when error occurs initialize the status and display toast message
            Status status = Autocomplete.getStatusFromIntent(data);
            Toast.makeText(getApplicationContext(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem){
        Intent backBtn = new Intent(getApplicationContext(), DashBoard.class);
        startActivity(backBtn);
        return true;
    }

    private void getUserCars(JSONArray j){
        //Traversing through all the items in the json array
        for(int i=0;i<j.length();i++){
            try {
                //Getting json object
                Log.e("jjjj",j.toString());
                JSONObject json = j.getJSONObject(i);
                System.out.println("carreg====>>>"+json.getString(Globalconstants.REG_NO));
                //Adding the name of the student to array list
                cars.add(json.getString(Globalconstants.REG_NO));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //Setting adapter to show the items in the spinner
        carservicespinner.setAdapter(new ArrayAdapter<String>(ServiceActivity.this, android.R.layout.simple_spinner_dropdown_item, cars));
    }

    private void getData(String userid){
        //Creating a string request
        StringRequest stringRequest = new StringRequest(Globalconstants.URL_USERCARS+"&userid="+userid,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONObject j = null;
                        try {
                            //Parsing the fetched Json String to JSON Object
                            j = new JSONObject(response);

                            //Storing the Array of JSON String to our JSON Array
                            result = j.getJSONArray(Globalconstants.JSON_ARRAY);
                            Log.e("results",result.toString());
                            //Calling method getStudents to get the students from the JSON Array
                            getUserCars(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //Creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }
}